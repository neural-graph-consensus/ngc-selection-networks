# NGC Selection Networks

![logo](logo.png)

Used at: https://gitlab.com/meehai/neo-vcl-iccv-2023

This repository is used to train selection (consensus) networks.

Basically, transform the input tensor `BxVxHxWxD` to output tensor `Bx1xHxWxD`, where B batch size, V is the number of voters, C is the number of output classes.

For now, it only supports `Semantic Segmentation` nodes, trained using cross entropy with input the single links and outptus the GT.

Usage

Train:
```
python train.py \
    /path/to/train_set \
    /path/to/validation_set \
    --config_path /path/to/cfg.yaml
```

Export:
```
python export.py \
    /path/to/test_set \
    --cfg_path /path/to/cfg.yaml \
    --weights_path /path/to/weights.ckpt \
    -o /path/to/export_dir \
    [--export_png]
    [--n_cores N]
```

The structure of `/path/to/train_set`:
```
- train_set
    path1
        1.npz, ..., N.npz
    ...
    path_n
        1.npz, ..., N.npz
    gt
        1.npz, ..., N.npz

```

For the test set, we have all the nodes, except the 'gt' node.
