"""Logistic Regression module"""
from torch import nn
import torch as tr


class LogisticRegression(nn.Module):
    """
    x :: (K, C), sum(x[i]) = 1
    gt :: (1, ) => onehot_gt :: (C, )
    x * w = x' :: (C, )
    loss = ce(x', onehot_gt)
    """

    def __init__(self, n_votes: int, n_classes: int):
        super().__init__()
        # self.fc = nn.Linear(n_votes * n_classes, n_classes, bias=False)
        self.w = nn.Parameter(tr.randn(n_votes, n_classes))
        self.n_votes = n_votes
        self.n_classes = n_classes

    def forward(self, x: tr.Tensor):
        # x:: mb, v, h, w, c
        y1 = tr.einsum("bvwhc, vc -> bwhc", x, self.w)
        return y1
