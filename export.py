#!/usr/bin/env python3
"""Voting networks exporter"""
from typing import Dict
from pathlib import Path
from copy import deepcopy
from argparse import ArgumentParser
from omegaconf import OmegaConf
from functools import partial
from multiprocessing import Pool, cpu_count
import shutil
import numpy as np
import torch as tr
from tqdm import tqdm
from torch import nn
from torch.utils.data import DataLoader, Subset
from lightning_module_enhanced import LME
from media_processing_lib.image import image_write
from ngclib.logger import logger
from ngclib.readers import NGCNpzReader

from models import build_model
from common import collate_fn, get_nodes, plot_fn


def get_args():
    parser = ArgumentParser()
    parser.add_argument("dataset_path", type=lambda x: Path(x).absolute(), help="Path to the ngc npz base dataset")
    parser.add_argument("--cfg_path", required=True, type=Path, help="Path to yaml cfg file for experiment")
    parser.add_argument("--output_path", "-o", type=lambda x: Path(x).absolute(), required=True)
    parser.add_argument("--weights_path", required=True, type=Path, help="Where to get the weights from")
    parser.add_argument("--n_cores", type=int, default=0)
    parser.add_argument("--export_png", action="store_true")
    parser.add_argument("--overwrite", action="store_true")

    args = parser.parse_args()
    args.n_cores = cpu_count() - 1 if args.n_cores == -1 else args.n_cores
    if args.output_path.exists():
        assert args.overwrite, f"--output_path '{args.output_path}' already exists. Use --overwrite!"
        logger.warning(f"Output path '{args.output_path}' exists. Overwriting")
        shutil.rmtree(args.output_path)

    assert args.cfg_path.exists(), args.cfg_path
    assert args.weights_path.exists(), args.weights_path
    return args

def worker_fn(indices: np.ndarray, model: nn.Module, reader: NGCNpzReader,
              out_path: Path, dataloader_params: Dict, export_png: bool):
    """Exports the indices of the reader to the out_path. Used in multiprocessing."""
    model = deepcopy(model)
    subset = Subset(reader, indices=indices)
    dataloader = DataLoader(subset, **dataloader_params)
    assert out_path.exists()
    for _, x in enumerate(tqdm(dataloader)):
        with tr.no_grad():
            y = model(x["data"])
        y = tr.softmax(y, axis=-1).to("cpu")
        out_files = [Path(f"{out_path}/{name}.npz") for name in x["name"]]
        if export_png:
            imgs = plot_fn(x["data"], y, x["labels"], reader.in_nodes)
        for i in range(len(out_files)):
            np.savez(out_files[i], y[i].numpy().astype(np.float16))
            if export_png:
                image_write(imgs[i], out_files[i].parent / "png" / f"{out_files[i].stem}.png")

def main():
    args = get_args()
    cfg = OmegaConf.load(args.cfg_path)
    nodes = get_nodes(args.dataset_path, [x for x in cfg.data.reader.get("nodes") if x != "gt"])

    logger.info(f"Dataset path: '{args.dataset_path}'")
    logger.info(f"Weights path: '{args.weights_path}'")
    logger.info(f"Output dir: '{args.output_path}'")

    # no out nodes for mode=export! (we can add it if we want to compare later)
    reader = NGCNpzReader(path=args.dataset_path, nodes=nodes, out_nodes=[])

    model = LME(build_model(cfg.model, len(reader.in_nodes)))
    model.load_state_dict(tr.load(args.weights_path, map_location="cpu")["state_dict"])
    args.output_path.mkdir(exist_ok=True, parents=True)
    (args.output_path / "png").mkdir(exist_ok=True, parents=True)

    # num_workers = 0 for n_cores > 0
    _collate_fn = partial(collate_fn, in_node_names=[n.name for n in reader.in_nodes], out_node_names=[])
    num_workers = cfg.data.loader_params.get("num_workers", 0) * (args.n_cores == 0)
    dataloader_params = {**cfg.data.loader_params, "num_workers": num_workers, "collate_fn": _collate_fn}
    fn = partial(worker_fn, model=model, reader=reader, out_path=args.output_path,
                 export_png=args.export_png, dataloader_params=dataloader_params)

    if args.n_cores > 0:
        chunks = np.array_split(np.arange(len(reader)), args.n_cores)
        pool = Pool(args.n_cores).map(fn, chunks)
        _ = list(pool)
    else:
        fn(indices=np.arange(len(reader)))


if __name__ == "__main__":
    main()
